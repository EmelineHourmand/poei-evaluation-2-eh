package com.zenika.poei.amismaisonduvin.repository;

import com.zenika.poei.amismaisonduvin.domain.Member;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface MemberRepository{

    List<Member> getAllMembers();
    Member getMemberById(int id);
    Member getMemberByPseudonym(String pseudonym);
    List<Member> getMemberByFirstName(String firstName);
    List<Member> getMemberByLastName(String lastName);
    Member saveMember(Member member);
    Member updateMember(int id, Member member);
    void deleteMember(int id);

}
