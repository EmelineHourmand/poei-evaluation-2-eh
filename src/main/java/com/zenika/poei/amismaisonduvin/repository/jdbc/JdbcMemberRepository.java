package com.zenika.poei.amismaisonduvin.repository.jdbc;


import com.zenika.poei.amismaisonduvin.domain.Member;
import com.zenika.poei.amismaisonduvin.repository.MemberRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class JdbcMemberRepository implements MemberRepository {

    private final JdbcTemplate jdbcTemplate;

    public static final RowMapper<Member> MEMBER_ROW_MAPPER = (rs, i) ->
            new Member(rs.getInt("id_member"), rs.getString("pseudonym"),
                    rs.getString("first_name"), rs.getString("last_name"));

    private static final String SQL_SELECT_MEMBERS = "SELECT * FROM members";
    private static final String SQL_INSERT_INTO_MEMBERS =
            "INSERT INTO members " +
            "(pseudonym, first_name, last_name)" +
            "VALUES (?, ?, ?) " +
            "RETURNING *";
    public static final String SQL_UPDATE_MEMBERS =
            "UPDATE members " +
            "SET pseudonym = ?, first_name = ?, last_name = ? " +
            "WHERE id_member = ? " +
            "RETURNING *";
    public static final String SQL_DELETE_MEMBERS = "DELETE FROM members WHERE id_member = ?";


    public JdbcMemberRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Member> getAllMembers() {
        return jdbcTemplate.query(SQL_SELECT_MEMBERS, MEMBER_ROW_MAPPER);
    }

    @Override
    public Member getMemberById(int id) {
        String sqlQuery = SQL_SELECT_MEMBERS + " WHERE id_member = ?";
        return jdbcTemplate.queryForObject(sqlQuery, MEMBER_ROW_MAPPER, id);
    }

    @Override
    public Member getMemberByPseudonym(String pseudonym) {
        String sqlQuery = SQL_SELECT_MEMBERS + " WHERE pseudonym = ?";
        return jdbcTemplate.queryForObject(sqlQuery, MEMBER_ROW_MAPPER, pseudonym);
    }

    @Override
    public List<Member> getMemberByFirstName(String firstName) {
        String sqlQuery = SQL_SELECT_MEMBERS + " WHERE first_name = ?";
        return jdbcTemplate.query(sqlQuery, MEMBER_ROW_MAPPER, firstName);
    }

    @Override
    public List<Member> getMemberByLastName(String lastName) {
        String sqlQuery = SQL_SELECT_MEMBERS + " WHERE last_name = ?";
        return jdbcTemplate.query(sqlQuery, MEMBER_ROW_MAPPER, lastName);
    }

    @Override
    public Member saveMember(Member member) {
        return jdbcTemplate.queryForObject(SQL_INSERT_INTO_MEMBERS, MEMBER_ROW_MAPPER,
                member.getPseudonym(),
                member.getFirstName(),
                member.getLastName());
    }

    @Override
    public Member updateMember(int id, Member member) {
        return jdbcTemplate.queryForObject(SQL_UPDATE_MEMBERS, MEMBER_ROW_MAPPER,
                member.getPseudonym(),
                member.getFirstName(),
                member.getLastName(),
                id);
    }

    @Override
    public void deleteMember(int id) {
        jdbcTemplate.update(SQL_DELETE_MEMBERS, id);
    }
}
