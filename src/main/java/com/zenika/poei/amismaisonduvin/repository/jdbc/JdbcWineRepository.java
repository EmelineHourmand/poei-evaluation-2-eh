package com.zenika.poei.amismaisonduvin.repository.jdbc;

import com.zenika.poei.amismaisonduvin.domain.Member;
import com.zenika.poei.amismaisonduvin.domain.Review;
import com.zenika.poei.amismaisonduvin.domain.Wine;
import com.zenika.poei.amismaisonduvin.repository.WineRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.time.Year;
import java.util.List;

@Component
public class JdbcWineRepository implements WineRepository {

    private final JdbcTemplate jdbcTemplate;

    public static final RowMapper<Wine> WINE_ROW_MAPPER = (rs, i) ->
            new Wine(rs.getInt("id_wine"),
                    rs.getString("appellation"),
                    rs.getString("region"),
                    Year.of(rs.getInt("year")),
                    rs.getString("colour"),
                    rs.getString("variety"));

    public static final RowMapper<Review> REVIEW_ROW_MAPPER_WITH_MEMBERS = (rs, i) ->
            new Review(
                    new Member(rs.getInt("id_member"),
                            rs.getString("pseudonym"),
                            rs.getString("first_name"),
                            rs.getString("last_name")),
                    rs.getInt("score"),
                    rs.getString("comment"));

    private static final String SQL_SELECT_WINES = "SELECT * FROM wines";

    public static final String SQL_SELECT_WINES_REVIEWS =
            "SELECT * FROM wines " +
            "LEFT JOIN reviews r on wines.id_wine = r.id_wine " +
            "LEFT JOIN members m on r.id_member = m.id_member " +
            "WHERE wines.id_wine = ?";

    private static final String SQL_INSERT_INTO_WINES =
            "INSERT INTO wines " +
            "(appellation, region, colour, variety, year)" +
            "VALUES (?, ?, ?, ?, ?) " +
            "RETURNING *";

    public static final String SQL_INSERT_INTO_REVIEWS =
            "INSERT INTO reviews " +
            "(id_member, id_wine, comment, score)" +
            "VALUES (?, ?, ?, ?) ";

    public static final String SQL_UPDATE_WINES =
            "UPDATE wines " +
            "SET appellation = ?, region = ?, colour = ?, variety = ?, year = ?" +
            "WHERE id_wine = ? " +
            "RETURNING *";

    public static final String SQL_DELETE_WINES = "DELETE FROM wines WHERE id_wine = ?";

    public JdbcWineRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Wine> getAllWines() {
        List<Wine> wines = jdbcTemplate.query(SQL_SELECT_WINES, WINE_ROW_MAPPER);
        for (Wine wine : wines) {
            wine.addReview(jdbcTemplate.queryForObject(SQL_SELECT_WINES_REVIEWS,
                    REVIEW_ROW_MAPPER_WITH_MEMBERS, wine.getId()));
        }
        return wines;
    }

    @Override
    public Wine getWineById(int id) {
        String sqlQuery = SQL_SELECT_WINES + " WHERE id_wine = ?";
        Wine wine = jdbcTemplate.queryForObject(sqlQuery, WINE_ROW_MAPPER, id);
        wine.addReview(jdbcTemplate.queryForObject(SQL_SELECT_WINES_REVIEWS,
                REVIEW_ROW_MAPPER_WITH_MEMBERS, wine.getId()));
        return wine;
    }

    @Override
    public List<Wine> getWineByRegion(String region) {
        String sqlQuery = SQL_SELECT_WINES + " WHERE region = ?";
        List<Wine> wines = jdbcTemplate.query(sqlQuery, WINE_ROW_MAPPER, region);
        for (Wine wine : wines) {
            wine.addReview(jdbcTemplate.queryForObject(SQL_SELECT_WINES_REVIEWS,
                    REVIEW_ROW_MAPPER_WITH_MEMBERS, wine.getId()));
        }
        return wines;
    }

    @Override
    public Wine saveWine(Wine wine) {
        return jdbcTemplate.queryForObject(SQL_INSERT_INTO_WINES, WINE_ROW_MAPPER,
                wine.getAppellation(),
                wine.getRegion(),
                wine.getColour(),
                wine.getVariety(),
                wine.getVintage().getValue());
    }

    @Override
    public Review saveReview(int id, Review review) {
        jdbcTemplate.update(SQL_INSERT_INTO_REVIEWS,
                review.getMember().getId(), id, review.getComment(), review.getScore());
        return review;
    }

    @Override
    public Wine updateWine(int id, Wine wine) {
        return jdbcTemplate.queryForObject(SQL_UPDATE_WINES, WINE_ROW_MAPPER,
                wine.getAppellation(),
                wine.getRegion(),
                wine.getColour(),
                wine.getVariety(),
                wine.getVintage().getValue(),
                id);
    }

    @Override
    public void deleteWine(int id) {
        jdbcTemplate.update(SQL_DELETE_WINES, id);
    }
}
