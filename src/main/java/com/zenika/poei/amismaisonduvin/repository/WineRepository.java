package com.zenika.poei.amismaisonduvin.repository;

import com.zenika.poei.amismaisonduvin.domain.Review;
import com.zenika.poei.amismaisonduvin.domain.Wine;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface WineRepository {

    List<Wine> getAllWines();
    Wine getWineById(int id);
    List<Wine> getWineByRegion(String region);
    Wine saveWine(Wine wine);
    Review saveReview(int idMember, Review review);
    Wine updateWine(int id, Wine wine);
    void deleteWine(int id);
}
