package com.zenika.poei.amismaisonduvin.controller.dto;

import com.zenika.poei.amismaisonduvin.domain.Member;

import java.util.ArrayList;
import java.util.List;

public class MemberDto {

    private String firstName;
    private String lastName;
    private String pseudonym;

    public String getPseudonym() {
        return pseudonym;
    }

    public void setPseudonym(String pseudonym) {
        this.pseudonym = pseudonym;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public static MemberDto fromDomain(Member member) {
        MemberDto memberDto = new MemberDto();
        memberDto.setPseudonym(member.getPseudonym());
        memberDto.setFirstName(member.getFirstName());
        memberDto.setLastName(member.getLastName());
        return memberDto;
    }

    public static List<MemberDto> listFromDomain(List<Member> members) {
        List<MemberDto> memberDtos = new ArrayList<>();
        for (Member member : members) {
            memberDtos.add(fromDomain(member));
        }
        return memberDtos;
    }

    public static Member fromController(MemberDto memberDto) {
        Member member = new Member();
        member.setPseudonym(memberDto.getPseudonym());
        member.setFirstName(memberDto.getFirstName());
        member.setLastName(memberDto.getLastName());
        return member;
    }

}
