package com.zenika.poei.amismaisonduvin.controller;

import com.zenika.poei.amismaisonduvin.controller.dto.MemberDto;
import com.zenika.poei.amismaisonduvin.domain.Member;
import com.zenika.poei.amismaisonduvin.repository.MemberRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/members")
public class MemberController {

    private final MemberRepository memberRepository;

    public MemberController(MemberRepository memberRepository) {
        this.memberRepository = memberRepository;
    }

    @GetMapping()
    public List<MemberDto> getMembers(MemberDto memberDto) {
        if (memberDto.getPseudonym() != null) {
            return List.of(MemberDto.fromDomain(memberRepository.getMemberByPseudonym(memberDto.getPseudonym())));
        } if (memberDto.getFirstName() != null) {
            return MemberDto.listFromDomain(memberRepository.getMemberByFirstName(memberDto.getFirstName()));
        } if (memberDto.getLastName() != null) {
            return MemberDto.listFromDomain(memberRepository.getMemberByLastName(memberDto.getLastName()));
        } else {
            return MemberDto.listFromDomain(memberRepository.getAllMembers());
        }
    }

    @GetMapping("/{id}")
    public MemberDto getMemberById(@PathVariable int id) {
        return MemberDto.fromDomain(memberRepository.getMemberById(id));
    }

    @PostMapping()
    public Member saveMember(@RequestBody MemberDto memberDto) {
        return memberRepository.saveMember(MemberDto.fromController(memberDto));
    }

    @PutMapping("/{id}")
    public Member updateMember(@PathVariable int id, @RequestBody MemberDto memberDto) {
        return memberRepository.updateMember(id, MemberDto.fromController(memberDto));
    }

    @DeleteMapping("/{id}")
    public void deleteMember(@PathVariable int id) {
        memberRepository.deleteMember(id);
    }
}
