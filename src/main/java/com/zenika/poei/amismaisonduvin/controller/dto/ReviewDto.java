package com.zenika.poei.amismaisonduvin.controller.dto;

import com.zenika.poei.amismaisonduvin.domain.Member;
import com.zenika.poei.amismaisonduvin.domain.Review;

public class ReviewDto {

    private int idMember;
    private String memberPseudo;
    private int score;
    private String comment;

    public int getIdMember() {
        return idMember;
    }

    public void setIdMember(int idMember) {
        this.idMember = idMember;
    }

    public String getMemberPseudo() {
        return memberPseudo;
    }

    public void setMemberPseudo(String memberPseudo) {
        this.memberPseudo = memberPseudo;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public static ReviewDto fromDomain(Review review) {
        ReviewDto reviewDto = new ReviewDto();
        reviewDto.setIdMember(review.getMember().getId());
        reviewDto.setMemberPseudo(review.getMember().getPseudonym());
        reviewDto.setComment(review.getComment());
        reviewDto.setScore(review.getScore());
        return reviewDto;
    }

    public static Review fromView(ReviewDto reviewDto) {
        Review review = new Review();
        Member member = new Member();
        member.setId(reviewDto.getIdMember());
        review.setMember(member);
        review.setComment(reviewDto.getComment());
        review.setScore(reviewDto.getScore());
        return review;
    }

}
