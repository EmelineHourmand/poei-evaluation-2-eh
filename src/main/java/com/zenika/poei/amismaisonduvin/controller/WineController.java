package com.zenika.poei.amismaisonduvin.controller;

import com.zenika.poei.amismaisonduvin.controller.dto.ReviewDto;
import com.zenika.poei.amismaisonduvin.controller.dto.WineDto;
import com.zenika.poei.amismaisonduvin.domain.Wine;
import com.zenika.poei.amismaisonduvin.domain.WineFactory;
import com.zenika.poei.amismaisonduvin.repository.WineRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/wines")
public class WineController {

    private final WineRepository wineRepository;

    public WineController(WineRepository wineRepository) {
        this.wineRepository = wineRepository;
    }

    @GetMapping()
    public List<WineDto> getWines(WineDto wineDto) {
        if (wineDto.getRegion() != null) {
            return WineDto.listFromDomain(wineRepository.getWineByRegion(wineDto.getRegion()));
        } else {
            return WineDto.listFromDomain(wineRepository.getAllWines());
        }
    }

    @GetMapping("/{id}")
    public WineDto getWineById(@PathVariable int id){
        return WineDto.fromDomain(wineRepository.getWineById(id));
    }

    @PostMapping()
    public WineDto saveWine(@RequestBody WineDto wineDto){
        return WineDto.fromDomain(wineRepository.saveWine(WineDto.fromController(wineDto)));
    }

    @PutMapping("/{id}")
    public WineDto updateWine(@PathVariable int id, @RequestBody WineDto wineDto) {
        return WineDto.fromDomain(wineRepository.updateWine(id, WineDto.fromController(wineDto)));
    }

    @DeleteMapping("/{id}")
    public void deleteWine(@PathVariable int id) {
        wineRepository.deleteWine(id);
    }

    @GetMapping("/init")
    public void initCatalog() {
        List<Wine> wines = WineFactory.addWineIntoBdd();
        for (Wine wine : wines) {
            saveWine(WineDto.fromDomain(wine));
        }
    }

    @PostMapping("/{id}/reviews")
    public ReviewDto saveReview(@PathVariable int id, @RequestBody ReviewDto reviewDto) {
        return ReviewDto.fromDomain(wineRepository.saveReview(id, ReviewDto.fromView(reviewDto)));
    }
}
