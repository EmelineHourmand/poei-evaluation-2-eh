package com.zenika.poei.amismaisonduvin.domain;

import java.time.Year;
import java.util.ArrayList;
import java.util.List;

/**
 * Wine catalog of the "Amis de la maison du vin".
 */
public final class WineFactory {

    // Colours
    public static final String ROUGE = "Rouge";
    public static final String BLANC = "Blanc";
    public static final String ROSE = "Rosé";

    // Regions
    public static final String VALLEE_DU_RHONE = "Vallée du Rhône";
    public static final String BORDELAIS = "Bordelais";
    public static final String PROVENCE = "Provence";
    public static final String LANGUEDOC_ROUSSILLON = "Languedoc-Roussillon";

    private WineFactory() {
        // Utility class
    }

    public static List<Wine> addWineIntoBdd() {
        List<Wine> wines = new ArrayList<>();
        wines.add(gigondas2019());
        wines.add(chateauneufDuPape2015());
        wines.add(moutonCadet2018());
        wines.add(graves2019());
        wines.add(saintJoseph2020());
        wines.add(cotesDeProvence2020());
        wines.add(rocheMazet2019());
        return wines;
    }

    public static Wine gigondas2019() {
        return new Wine("Gigondas", Year.of(2019)
                , VALLEE_DU_RHONE, ROUGE, "Grenache");
    }

    public static Wine chateauneufDuPape2015() {
        return new Wine("Châteauneuf-du-Pape", Year.of(2015)
                , VALLEE_DU_RHONE, ROUGE, "Syrah");
    }

    public static Wine moutonCadet2018() {
        return new Wine("Mouton Cadet", Year.of(2018)
                , BORDELAIS, ROUGE, "Merlot");
    }

    public static Wine graves2019() {
        return new Wine("Graves", Year.of(2019)
                , BORDELAIS, BLANC, "Sauvignon");
    }

    public static Wine saintJoseph2020() {
        return new Wine("Saint-Joseph", Year.of(2020)
                , VALLEE_DU_RHONE, BLANC, "Roussanne");
    }

    public static Wine cotesDeProvence2020() {
        return new Wine("Côtes de Provence", Year.of(2020)
                , PROVENCE, ROSE, "Grenache");
    }

    public static Wine rocheMazet2019() {
        return new Wine("Roche Mazet", Year.of(2019)
                , LANGUEDOC_ROUSSILLON, ROSE, "Grenache");
    }
}
