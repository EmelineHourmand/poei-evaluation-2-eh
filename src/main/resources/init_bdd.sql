create table IF NOT EXISTS  bdd.members
(
    id_member  serial
        constraint members_pk
            primary key,
    pseudonym  text not null,
    first_name text not null,
    last_name  text not null
);

alter table bdd.members
    owner to postgres;

create unique index members_id_member_uindex
    on bdd.members (id_member);

create unique index members_pseudonym_uindex
    on bdd.members (pseudonym);

create table IF NOT EXISTS  bdd.wines
(
    id_wine     serial
        constraint wines_pk
            primary key,
    appellation text    not null,
    region      text    not null,
    colour      text    not null,
    variety     text,
    year        integer not null
);

alter table bdd.wines
    owner to postgres;

create unique index wines_id_wine_uindex
    on bdd.wines (id_wine);

create table IF NOT EXISTS  bdd.reviews
(
    id_member integer not null
        constraint review_members_id_member_fk
            references bdd.members,
    id_wine   integer not null
        constraint review_wines_id_wine_fk
            references bdd.wines,
    comment   text,
    score     integer not null
);

alter table bdd.reviews
    owner to postgres;

INSERT INTO members (pseudonym, first_name, last_name)
VALUES ('george-seche', 'Alexsandre', 'Baron');

